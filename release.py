import argparse
import os

import requests

def release(token, tag=None):
    headers = {
        "PRIVATE-TOKEN": token
    }

    tag = tag or os.environ.get('CI_COMMIT_TAG')

    data = {
        "name": f"Release {tag}",
        "tag_name": tag,
        "description": "blabla"
    }

    project_id = int(os.environ.get('CI_PROJECT_ID'))
    url = f"https://gitlab.com/api/v4/projects/{project_id}/releases"
    r = requests.post(url, data=data, headers=headers)
    return r

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--token")
    parser.add_argument("--tag")
    args = parser.parse_args()
    r = release(args.token, tag=args.tag)
    assert r.status_code == 201, f"Release failed, {r.content}"