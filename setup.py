from setuptools import setup, find_packages

setup(
    name="My package",
    version="0.1.1",
    author="Moi",
    packages=find_packages(),
    use_scm_version=True,
    setup_requires=['setuptools_scm']
)